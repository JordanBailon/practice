package com.barber.sugarorm;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText titulo, edicion;
    Button guardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        titulo = findViewById(R.id.titulo);
        edicion = findViewById(R.id.edicion);
        guardar = findViewById(R.id.guardar);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book registro = new Book (
                    titulo.getText().toString(),
                            edicion.getText().toString());
                registro.save();
                Log.e("guardar"+ titulo.getText().toString()+ edicion.getText().toString(), "Datos Guardados");

            }
        });
    }
}
